package com.devcamp.j02_javabasic.s50;

public class WrapperExample {
    public static void autoBoxing() {
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 40;
        float fat = 50.0f;
        double dbl = 60.00;
        char ch = 'a';
        boolean bool = true;
        /*
         * Continue
         */
        Byte byeobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("Printing object value (in giá trị của object...");
        System.out.println("Byte object: " + byeobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Interger object " + intobj);
        System.out.println("Double object " + doubleobj);
        System.out.println("Long object " + longobj);
        System.out.println("Charater object " + charobj);
        System.out.println("Float object " + floatobj);
        System.out.println("Boolean object " + boolobj);

    }
    public static void unBoxing() {
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 40;
        float fat = 50.0f;
        double dbl = 60.00;
        char ch = 'a';
        boolean bool = true;
        /*
         * Continue
         */
        Byte byeobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        byte bytevalue = byeobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;

        System.out.println("Printing object value (in giá trị của object...");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("interger value " + intvalue);
        System.out.println("double value " + doublevalue);
        System.out.println("long value " + longvalue);
        System.out.println("charater value " + charvalue);
        System.out.println("float value " + floatvalue);
        System.out.println("boolean value " + boolvalue);

    }
    public static void main(String[] args){
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
}
